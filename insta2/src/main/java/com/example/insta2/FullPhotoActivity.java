package com.example.insta2;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class FullPhotoActivity extends AppCompatActivity {

    ImageView ivFullImage;
    TextView tvFullImageText;
    private Matrix matrix = new Matrix();
    private float scale = 1f;
    private ScaleGestureDetector scaleGestureDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_photo);

        ivFullImage = (ImageView) findViewById(R.id.ivFullImage);
        tvFullImageText = (TextView) findViewById(R.id.tvFullImageText);


        //Getting Image from ID
        Intent intent = getIntent();
        long id = intent.getLongExtra("_ImageID", -1);
        if (id == -1) {
            Toast.makeText(this, "Something wrong", Toast.LENGTH_SHORT).show();
        } else {
            Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Long.toString(id));
            try {
                ivFullImage.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), uri));

                //Get the name of picture
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                String[] projection = new String[]{MediaStore.Images.Media.DISPLAY_NAME};
                Cursor cursorName = getContentResolver().query(uri, projection,
                        MediaStore.Images.Media._ID + " = ?", new String[]{String.valueOf(id)}, null);
                if (cursorName.moveToFirst()) {
                    tvFullImageText.setText(cursorName.getString(cursorName.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)));
                } else {
                    tvFullImageText.setText("");
                }
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }


        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());


    }

    private class ScaleListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            scale = Math.max(0.1f, Math.min(scale, 5.0f));
            matrix.setScale(scale, scale);
            ivFullImage.setImageMatrix(matrix);

            float newWidth = ivFullImage.getWidth() * scale;
            float newHeight = ivFullImage.getHeight() * scale;
            float dX = ivFullImage.getWidth() - newWidth;
            float dY = ivFullImage.getHeight() - newHeight;

            //matrix.setTranslate(dX, dY);



            //ivFullImage.setTranslationX(-((newWidth - ivFullImage.getWidth())/2));
            // matrix.setTranslate(((ivFullImage.getWidth() - newWidth) / 2), 0);

            //Log.d("myLog", "getX = " + ivFullImage.getX() + "; getY = " + ivFullImage.getY() );
           // Log.d("myLog", "getTranslationX = " + ivFullImage.getTranslationX() + "; getTranslationY = " + ivFullImage.getTranslationY());
            Log.d("myLog", "ivFullImage.getWidth() = " + ivFullImage.getMatrix() + "; ivFullImage.getHeight = " + ivFullImage.getBottom());
            // ivFullImage.getTop()
            ivFullImage.setImageMatrix(matrix);
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }


}
