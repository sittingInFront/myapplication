package com.example.activityresult;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Second extends AppCompatActivity {

    TextView tvA;
    TextView tvB;
    Button btnSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        tvA = (TextView) findViewById(R.id.tvA);
        tvB = (TextView) findViewById(R.id.tvB);
        btnSum = (Button) findViewById(R.id.btnSum);

        final Intent intent = getIntent();

        tvA.setText(intent.getStringExtra("et1"));
        tvB.setText(intent.getStringExtra("et2"));

        btnSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent();
                intent1.putExtra("sum", Integer.valueOf(tvA.getText().toString()) + Integer.valueOf(tvB.getText().toString()));
                setResult(RESULT_OK, intent1);
                finish();
            }
        });

    }
}
