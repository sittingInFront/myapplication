package com.example.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import java.util.Locale;



/**
 *
 * Created by alex on 12.07.2017.
 */

public class CustomDialogFragment extends DialogFragment {

    public CustomDialogFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog, container);

        getDialog().setTitle("title)");
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setGravity(Gravity.BOTTOM);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutParams = getDialog().getWindow().getAttributes();
        //layoutParams.horizontalMargin = 0;
        //layoutParams.horizontalWeight = 1;
        //layoutParams.x = 0;
        //layoutParams.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE;

        getDialog().getWindow().setAttributes(layoutParams);
        Log.d("myLog", String.valueOf(layoutParams.width ));

        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        Log.d("myLog", "onCreateView");

        //in API26 the casting below is redundant
        Button btn = (Button) view.findViewById(R.id.btnChoose);
        /*final*/ EditText etText = (EditText) view.findViewById(R.id.etText);
        RadioGroup rgLang = (RadioGroup) view.findViewById(R.id.rgLang);

        btn.setOnClickListener(new View.OnClickListener() {
            private EditText et;
            private RadioGroup rgLang;

            public View.OnClickListener init(EditText et, RadioGroup rgLang){
                this.et = et;
                this.rgLang = rgLang;
                return this;
            }

            @Override
            public void onClick(View view) {
                ReceivingData receivingData = (ReceivingData) getActivity();

                if (!TextUtils.isEmpty(et.getText().toString())){
                    switch (rgLang.getCheckedRadioButtonId()){
                        case R.id.rbEn:
                            receivingData.putData(new Locale("en"), et.getText().toString());
                            break;
                        case R.id.rbRu:
                            receivingData.putData(new Locale("ru"), et.getText().toString());
                            break;
                    }
                }
                dismiss();
            }
        }.init(etText, rgLang));
        return view;
    }

}
