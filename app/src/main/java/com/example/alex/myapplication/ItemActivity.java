package com.example.alex.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ItemActivity extends AppCompatActivity {

    TextView tvProductName;
    TextView tvProductDescription;
    TextView tvProductPrice;
    ImageView ivProductImage;
    ImageView ivProductFavorite;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        tvProductName = (TextView)findViewById(R.id.tvProductName);
        tvProductDescription = (TextView)findViewById(R.id.tvProductDescription);
        tvProductPrice = (TextView)findViewById(R.id.tvPrice);
        ivProductImage = (ImageView)findViewById(R.id.ivProduct);
        ivProductFavorite = (ImageView)findViewById(R.id.ivFavorite);

        Intent intent = getIntent();
        //Toast.makeText(this, ((Product)intent.getSerializableExtra("product")).getName().toString(), Toast.LENGTH_SHORT).show();
        Product product = (Product) intent.getSerializableExtra("product");
        tvProductName.setText(product.getName());
        tvProductPrice.setText(String.valueOf(product.getPrice()));
        tvProductDescription.setText(product.getDescription());
        ivProductImage.setImageResource(product.getResDrawableId());
        //Long itemID = intent.getLongExtra("itemID", 0);
        //int position = intent.getIntExtra("position", 0);

        //productName.setText(products.get(position).getName());


    }
}
