package com.example.alex.myapplication;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.drawable.Drawable;
import android.widget.Toast;


import java.util.List;

/**
 * Created by ALEX on 18.06.2017.
 */

public class ProductAdapter extends ArrayAdapter {

    int resource;
    Context context;

    public ProductAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        final Product product;

        if (convertView == null){
            view = View.inflate(getContext(), resource, null);
        }else {
            view = convertView;
        }

        product = (Product) getItem(position);
        TextView productPrice = (TextView)view.findViewById(R.id.tvPrice);
        productPrice.setText(String.valueOf(product.getPrice()));
        ImageView imageView = (ImageView)view.findViewById(R.id.ivProduct);

       //Log.d("myLogs", "VectorDrawable(adapter) = " + product.drawable.getCurrent() );
        imageView.setImageResource(product.getResDrawableId());
        Log.d("myLogs", "imageView.getId() = " + imageView.getId() );
        //imageView.setImageResource(R.drawable.ic_assignment_returned_black_24dp);
        TextView productName = (TextView)view.findViewById(R.id.tvProductName);
        productName.setText(product.getName());
        TextView productDescription = (TextView)view.findViewById(R.id.tvProductDescription);
        productDescription.setText(product.getDescription());
        ImageView imageView1 = (ImageView)view.findViewById(R.id.ivFavorite);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, product.getName(), Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
}
