package com.example.alex.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.VectorDrawable;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView llList;
    ArrayList<Product> products;
    Menu mainMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Some action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        llList = (ListView) findViewById(R.id.llList);
        products = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Product product = new Product("Name " + i, 100 + i, R.drawable.ic_accessibility_black_24dp,
                    false, "Description " + i);
            products.add(product);
        }

        ProductAdapter productAdapter = new ProductAdapter(this, R.layout.ll_kufar, products);
        llList.setAdapter(productAdapter);
        llList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ItemActivity.class);
                intent.putExtra("itemID", id);
                intent.putExtra("position", position);
                intent.putExtra("drawable", products.get(position).getResDrawableId());
                intent.putExtra("product", products.get(position));
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        //menu.add(1,1,5,"Programme created").setCheckable(true);
        mainMenu = menu;
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.menu_add:
                Toast.makeText(MainActivity.this, "Menu add", Toast.LENGTH_SHORT).show();
                break;


        }
        return super.onOptionsItemSelected(item);
    }


}

