package com.example.simplefragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by alex on 19.07.2017.
 */

public class Fragment2 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f2, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.ivF21);
        ImageView imageView1 = (ImageView) view.findViewById(R.id.ivF22);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReceiveData receiveData = (ReceiveData) getActivity();
                receiveData.empty2();
            }
        });

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReceiveData receiveData = (ReceiveData) getActivity();
                receiveData.empty2();
            }
        });


        return view;
    }

    public static Fragment2 newInstance() {

        Bundle args = new Bundle();

        Fragment2 fragment = new Fragment2();
        fragment.setArguments(args);
        return fragment;
    }
}
