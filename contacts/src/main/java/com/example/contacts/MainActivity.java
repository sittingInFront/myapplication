package com.example.contacts;

import android.Manifest;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public final static String MY_CONTACT_NAME = "contactName";
    public final static String MY_CONTACT_MOBILE_PHONE_NUMBER = "contactMobileNumber";

    public final static String MY_LOG_TAG = "Log_Contacts";

    ListView listView;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.lvList);
        btnAdd = (Button) findViewById(R.id.btnAdd);

        //check permission
        int permissionReadContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        int permissionWriteContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
        if (permissionReadContacts == PackageManager.PERMISSION_GRANTED
                && permissionWriteContacts == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
            loadContacts();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS,
                    Manifest.permission.WRITE_CONTACTS}, 1);


        }


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
                startActivityForResult(intent, 0);

            }
        });
    }

    private void loadContacts() {
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        MyCursorAdapter myCursorAdapter = new MyCursorAdapter(this, cursor, true);
        listView.setAdapter(myCursorAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults != null && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            loadContacts();
        }
    }

    private void addContact(String displayName, String phoneNumber) {
//        Uri rawContractUri = getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI,
//                new ContentValues());
//        //get ID
//        long rawContractID = ContentUris.parseId(rawContractUri);
//        ContentValues contentValues = new ContentValues();
//        //Bind account with data
//        contentValues.put(ContactsContract.Contacts.Data.RAW_CONTACT_ID, rawContractID);
//        //set MIME type for data
//        contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
//        //set Name
//        contentValues.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, displayName);
//        getContentResolver().insert(ContactsContract.Data.CONTENT_URI,contentValues);

        ArrayList<ContentProviderOperation> op = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = op.size();


/* Добавляем пустой контакт */
        op.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
/* Добавляем данные имени */
        op.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, displayName)
                .build());
/* Добавляем данные телефона */
        op.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());

        try {
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, op);
        } catch (Exception e) {
            Log.e("Exception: ", e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String contactName, contactMobileNumber;
        if (requestCode == 0 && resultCode == RESULT_OK) {
            contactName = data.getStringExtra(MY_CONTACT_NAME);
            contactMobileNumber = data.getStringExtra(MY_CONTACT_MOBILE_PHONE_NUMBER);
            if (contactName != null && contactMobileNumber != null) {
                Toast.makeText(this, "contactName: " + contactName, Toast.LENGTH_SHORT).show();
                Log.d(MY_LOG_TAG, "ContactName: " + contactName + "; ContactMobileNumber: " + contactMobileNumber);
                addContact(contactName, contactMobileNumber);
            }

        }
    }
}
