package com.example.simplefragment;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements ReceiveData {

    Button btnF1;
    Button btnF2;
    Button btnF3;
    TabLayout tl1;
    ViewPager vpFragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnF1 = (Button) findViewById(R.id.btnF1);
        btnF2 = (Button) findViewById(R.id.btnF2);
        btnF3 = (Button) findViewById(R.id.btnF3);
        vpFragmentContainer = (ViewPager) findViewById(R.id.vpFragmentContainer);
        tl1 = (TabLayout) findViewById(R.id.tl1);


        tl1.setupWithViewPager(vpFragmentContainer,true);

        vpFragmentContainer.setAdapter( new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "Tab1";

                    case 1:
                        return "Tab2";

                    case 2:
                        return "Tab3";

                }

                return "Tab13";
            }

            @Override
            public Fragment getItem(int position) {

                switch (position) {
                    case 0:
                        return Fragment1.newInstance();

                    case 1:
                        return Fragment2.newInstance();

                    case 2:
                        return Fragment3.newInstance();

                }

                return Fragment1.newInstance();
            }

            @Override
            public int getCount() {
                return 3;
            }
        });

      /*  btnF1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment(Fragment1.newInstance());

            }
        });
        btnF2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment(Fragment2.newInstance());
            }
        });
        btnF3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment(Fragment3.newInstance());
            }
        });*/


    }

    public void startFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.vpFragmentContainer, fragment).commit();
    }

    @Override
    public void empty1() {
        btnF1.setBackgroundColor(Color.RED);
    }

    @Override
    public void empty2() {
        btnF2.setBackgroundColor(Color.BLUE);
    }

    @Override
    public void empty3() {
        btnF3.setBackgroundColor(Color.CYAN);
    }

    public class FragmentPagerAdapter1 extends FragmentPagerAdapter {

        public FragmentPagerAdapter1(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return 0;
        }
    }
}
