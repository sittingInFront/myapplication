package com.example.simplefragment;

/**
 * Created by alex on 19.07.2017.
 */

interface ReceiveData {
    public void empty1();
    public void empty2();
    public void empty3();
}
