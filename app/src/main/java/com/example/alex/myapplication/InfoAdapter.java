package com.example.alex.myapplication;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ALEX on 14.06.2017.
 */

public class InfoAdapter extends ArrayAdapter {

    int resource;


    public InfoAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Info> objects) {

        super(context, resource, objects);
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        Info pos;
        if (convertView == null) {
            view = View.inflate(getContext(), resource, null);
        } else {
            view = convertView;
        }
        pos = (Info) getItem(position);

        TextView number = (TextView) view.findViewById(R.id.tvNumber);
        number.setText((String.valueOf(pos.getNumber())));
        TextView text1 = (TextView) view.findViewById(R.id.tvText);
        text1.setText(pos.getText1());
        return view;
    }
}
