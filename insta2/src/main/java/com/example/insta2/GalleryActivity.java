package com.example.insta2;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class GalleryActivity extends AppCompatActivity {
    ListView lvGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        final String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Thumbnails._ID};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        lvGallery = (ListView) findViewById(R.id.lvGallery);
        MyCursorAdapter myCursorAdapter = new MyCursorAdapter(this, cursor, true);
        lvGallery.setAdapter(myCursorAdapter);

        lvGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(GalleryActivity.this, parent.toString() + ";|||  " + lvGallery.toString(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), FullPhotoActivity.class);
                intent.putExtra("_ImageID", id);
                startActivity(intent);
            }
        });
    }
}

