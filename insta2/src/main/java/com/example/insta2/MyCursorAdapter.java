package com.example.insta2;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by alex on 05.07.2017.
 */

public class MyCursorAdapter extends CursorAdapter {
    LayoutInflater layoutInflater;
    ContentResolver contentResolver;

    public MyCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentResolver = context.getContentResolver();
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView ivThumbnails;
        TextView tvName;

        ivThumbnails = (ImageView) view.findViewById(R.id.ivThumbnails);
        tvName = (TextView) view.findViewById(R.id.tvName);

        tvName.setText(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)));

        int imageID = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID));

        Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(imageID));

        try {
//            ivThumbnails.setImageBitmap(MediaStore.Images.Thumbnails.getThumbnail(context.getContentResolver(),
//                    imageID, MediaStore.Images.Thumbnails.MICRO_KIND, null));
            ivThumbnails.setImageBitmap(MediaStore.Images.Media.getBitmap(contentResolver, uri));
        } catch (Exception e) {
            Toast.makeText(context, uri.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(R.layout.gallery_item, parent, false);
    }
}
