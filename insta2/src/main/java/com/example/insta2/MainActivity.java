package com.example.insta2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    Button btnGetPicture;
    ImageView ivImage;
    Button btnChangeFrame;
    Button btnGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int permissionReadExStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionManageDocuments = ContextCompat.checkSelfPermission(this, Manifest.permission.MANAGE_DOCUMENTS);
        if (permissionReadExStorage == PackageManager.PERMISSION_GRANTED
                && permissionManageDocuments == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.MANAGE_DOCUMENTS}, 1);
        }


        btnGetPicture = (Button) findViewById(R.id.btnGetPicture);
        btnChangeFrame = (Button) findViewById(R.id.btnChangeFrame);
        btnGallery = (Button) findViewById(R.id.btnGallery);

        ivImage = (ImageView) findViewById(R.id.ivImage);

        btnGetPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        btnChangeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (ivImage.getBackground() != getDrawable(R.drawable.frame).hashCode()) {
//                    ivImage.setBackgroundResource(R.drawable.ex6);
//                } else {
//                    ivImage.setBackgroundResource(R.drawable.frame);
//                }

            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            Bitmap bitmap = (Bitmap) bundle.get("data");
            ivImage.setImageBitmap(bitmap);
        }
    }

    @Override
    public int getRequestedOrientation() {
        return super.getRequestedOrientation();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults == null || grantResults[0] == PackageManager.PERMISSION_DENIED) {
            btnGallery.setEnabled(false);
        }
    }


}
