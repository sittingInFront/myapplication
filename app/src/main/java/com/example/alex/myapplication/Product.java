package com.example.alex.myapplication;


import android.graphics.drawable.VectorDrawable;
import android.media.Image;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * Created by ALEX on 18.06.2017.
 */

public class Product implements Serializable{
    private String name;
    private int price;
    private Image image;
    private VectorDrawable drawable;
    private int resDrawableId;
    private boolean favorite;
    private String description;
    private Date date;
    private Time time;

    public Product(String name, int price, int resDrawableId, boolean favorite, String description) {
        this.name = name;
        this.price = price;
        //this.drawable = drawable;
        this.favorite = favorite;
        this.description = description;
        this.resDrawableId = resDrawableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public VectorDrawable getDrawable() {
        return drawable;
    }

    public void setDrawable(VectorDrawable drawable) {
        this.drawable = drawable;
    }

    public int getResDrawableId() {
        return resDrawableId;
    }

    public void setResDrawableId(int resDrawableId) {
        this.resDrawableId = resDrawableId;
    }
}
