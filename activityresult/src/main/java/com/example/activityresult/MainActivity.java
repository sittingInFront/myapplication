package com.example.activityresult;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText et1;
    EditText et2;
    Button btnPush;
    TextInputLayout tilEt1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Toast.makeText(this, String.valueOf(data.getIntExtra("sum", 0)), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(et1.getText().toString())) {
                    tilEt1.setError("Enter data!!!");
                } else {
                    tilEt1.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tilEt1 = (TextInputLayout) findViewById(R.id.tilEt1);
        btnPush = (Button) findViewById(R.id.btnPush);
        btnPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et1.getText().toString())) {
                    tilEt1.setError("Enter data!!!");
                } else {
                    Intent intent = new Intent(MainActivity.this, Second.class);
                    intent.putExtra("et1", et1.getText().toString());
                    intent.putExtra("et2", et2.getText().toString());
                    startActivityForResult(intent, 1);
                }
            }
        });


    }
}
