package com.example.dialog;

import java.util.Locale;

/**
 * Created by alex on 13.07.2017.
 */

public interface ReceivingData {

    void putData(Locale locale, String text);
}
