package com.example.contacts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddContactActivity extends AppCompatActivity {

    Button btnAddContact;
    EditText etName;
    EditText etMobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        btnAddContact = (Button) findViewById(R.id.btnAddContact);
        etName = (EditText) findViewById(R.id.etContactName);
        etMobileNumber = (EditText) findViewById(R.id.etContactMobileNumber);

        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etName.getText().toString())
                        || TextUtils.isEmpty(etMobileNumber.getText().toString())   ){
                    Toast.makeText(AddContactActivity.this, "Enter data", Toast.LENGTH_SHORT).show();
                } else{
                    Intent intent = new Intent();
                    intent.putExtra("contactName", etName.getText().toString());
                    intent.putExtra("contactMobileNumber", etMobileNumber.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }
}
