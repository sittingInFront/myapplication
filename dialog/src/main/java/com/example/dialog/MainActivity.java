package com.example.dialog;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ReceivingData {

    Button btnDialog;
    Button btnDialogFragment;
    DialogFragment dialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDialog = (Button) findViewById(R.id.btnDialog);
        btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).setTitle(R.string.title)
                        .setMessage(R.string.message)
                        .setPositiveButton(R.string.english, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                setLocale(new Locale("en"));
                            }
                        })
                        .setNegativeButton(R.string.russian, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                setLocale(new Locale("ru"));
                            }
                        })

                        .create();
                alertDialog.show();
            }
        });

        dialogFragment = new CustomDialogFragment();
        btnDialogFragment = (Button) findViewById(R.id.btnDialogFragment);
        btnDialogFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFragment.setStyle(R.style.CustomDialog, R.style.DialogTheme);
                dialogFragment.show(getFragmentManager(), "dialogFragment");
            }
        });
    }

    private void setLocale(Locale en) {
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(en);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        recreate();
    }

    @Override
    public void putData(Locale locale, String text) {
        setLocale(locale);
        Toast.makeText(this, "Text from Fragment: " + text, Toast.LENGTH_SHORT).show();
    }


}
