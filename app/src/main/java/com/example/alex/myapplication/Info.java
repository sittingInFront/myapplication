package com.example.alex.myapplication;

/**
 * Created by ALEX on 14.06.2017.
 */

public class Info {
    int Number;
    String text1;
    String text2;
    String text3;

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public Info(int number, String text1, String text2, String text3) {
        Number = number;
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
    }
}
