package com.example.contacts;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ALEX on 28.06.2017.
 */

public class MyCursorAdapter extends CursorAdapter {

    LayoutInflater layoutInflater;

    public MyCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return layoutInflater.inflate(R.layout.item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tv1;
        TextView tvPhone;

        tv1 =(TextView) view.findViewById(R.id.tvName);
        tvPhone =(TextView) view.findViewById(R.id.tvPhone);

        tv1.setText(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)));

        String contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

        //getting !!!ONLY mobile phone. In other case we receive empty cursor.
        //if it needs more types of number should define more ContactsContract.CommonDataKinds.Phone.TYPE_
        Cursor cursorPhoneNumber = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " =  ? AND "
                    + ContactsContract.CommonDataKinds.Phone.TYPE  + " = "
                    + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                    new String[]{contactID},
                    null);
        String contactNumber = "";
        if (cursorPhoneNumber.moveToFirst() ){
            contactNumber = cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        } else {
            //if contact don't have mobile number
            //Toast.makeText(context, cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)), Toast.LENGTH_SHORT).show();

        }

        if (contactNumber != null)
        {
            tvPhone.setText(contactNumber);
        }

    }
}
